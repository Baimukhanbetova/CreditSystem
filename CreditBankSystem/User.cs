﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditBankSystem
{
    public class User
    {
        public long IIN { get; set;}
        public string FirstName { get; set;}
        public string LastName { get; set; }
        public string Password { get; set; }
        public bool Indebtedness { get; set;}
    }
}
