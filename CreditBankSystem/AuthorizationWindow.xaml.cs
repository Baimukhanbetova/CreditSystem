﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CreditBankSystem
{
    /// <summary>
    /// Логика взаимодействия для AuthorizationWinwod.xaml
    /// </summary>
    public partial class AuthorizationWindow : Window
    {
        Statement application = new Statement();
        MainWindow main = new MainWindow(); 
        public AuthorizationWindow()
        {
            InitializeComponent();
        }

        private void SingInButton_Click(object sender, RoutedEventArgs e)
        {
            int login = Convert.ToInt32(textBoxForLogin.Text);
            for (int i = 0; i < main.listOfUser.Count; i++)
            {
                if (login == main.listOfUser[i].IIN && textBoxForLogin != null && textBoxForPassword.Text.ToString().Equals(main.listOfUser[i].Password))
                {
                    application.Owner = this;
                    application.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    application.Show();
                    application.nameTextBox.Text = main.listOfUser[i].FirstName;
                    application.lastNameTextBox.Text = main.listOfUser[i].LastName;
                    application.iinTextBox.Text = main.listOfUser[i].IIN.ToString();
                    if (application.IsActive)
                        break;
                }
            }
        }
    }
}
