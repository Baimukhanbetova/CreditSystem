﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreditBankSystem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       public List<User> listOfUser;
        public MainWindow()
        {
            InitializeComponent();
            listOfUser = new List<User>
            {
                new User
                {
                    IIN = 00012345678,
                    FirstName="Assel",
                    LastName="Baimukhanbetova",
                    Password="v4v4v4",
                    Indebtedness=false
                },

                   new User
                {
                    IIN = 00012345677,
                    FirstName="Aleksei",
                    LastName="Baimukhanbetov",
                    Password="v4v4v5",
                    Indebtedness=true
                },

                    new User
                {
                    IIN = 00012345676,
                    FirstName="Aleksandr",
                    LastName="Baimukhanbetov",
                    Password="v4v4v6",
                    Indebtedness=false
                },
            };
        }

        private void СheckInButton_Click(object sender, RoutedEventArgs e)
        {
            int password = Convert.ToInt32(textBoxForPassword.Text);
              for (int i = 0; i < listOfUser.Count; i++)
            {
                if (password==listOfUser[i].IIN)
                {
                    SetPassword openWindow = new SetPassword();
                    openWindow.Owner = this;
                    openWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    openWindow.Show();
                    openWindow.textBoxForName.Text = listOfUser[i].FirstName;
                    openWindow.textBoxForLastName.Text = listOfUser[i].LastName;
                    openWindow.textBoxForPassword.Text = listOfUser[i].Password;
                    if (openWindow.IsActive)
                        break;
                }
                else
                    MessageBox.Show("Somethig is wrong! TryAgain");
            }
        }
    }
}
