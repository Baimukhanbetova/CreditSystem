﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CreditBankSystem
{
    /// <summary>
    /// Логика взаимодействия для Application.xaml
    /// </summary>
    public partial class Statement : Window
    {
        MainWindow main = new MainWindow();
        public Statement()
        {
            InitializeComponent();
        }

        private void AnswerButton_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < main.listOfUser.Count; i++)
            {
                if (main.listOfUser[i].Indebtedness)
                {
                    MessageBox.Show("К сожалению, у Вас имеется задолженость, Вам отказано в запросе!");
                    break;
                }
                else
                {
                    MessageBox.Show("Вам одобрен кредит, заберите деньги!");
                    break;
                }

            }
        }
    }
}
